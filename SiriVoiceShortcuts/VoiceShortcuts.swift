import IntentsUI

struct VoiceShortcuts {

    static func getExistingVoiceShortcut(with identifier: String, completion: @escaping (INVoiceShortcut?) -> Void) {

        INVoiceShortcutCenter.shared.getAllVoiceShortcuts { shortcuts, _ in

            if let shortcuts = shortcuts {

                let existing = shortcuts.first(where: { voiceShortcut -> Bool in

                    let shortcut = voiceShortcut.shortcut

					/*
					error: value of type 'INVoiceShortcut' has no member 'shortcut'; did you mean '__shortcut'?
					                    let shortcut = voiceShortcut.shortcut
					                                   ^~~~~~~~~~~~~ ~~~~~~~~
					                                                 __shortcut
					Intents.INVoiceShortcut:6:25: note: '__shortcut' declared here
					    @NSCopying open var __shortcut: INShortcut { get }
					*/

                    return shortcut.userActivity?.activityType == identifier
                })

                completion(existing)
                return
            }

            completion(nil)
        }
    }
}
